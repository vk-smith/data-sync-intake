import boto3
import json
import time
from chalicelib import config

settings = config.get_config()

kinesis_client = boto3.client('kinesis',
                              region_name=settings['region_name'])

response = kinesis_client.describe_stream(StreamName=settings['stream_name'])

my_shard_id = response['StreamDescription']['Shards'][0]['ShardId']

shard_iterator = kinesis_client.get_shard_iterator(StreamName=settings['stream_name'],
                                                   ShardId=my_shard_id,
                                                   ShardIteratorType='LATEST')

my_shard_iterator = shard_iterator['ShardIterator']

record_response = kinesis_client.get_records(ShardIterator=my_shard_iterator,
                                             Limit=2)

while 'NextShardIterator' in record_response:
    record_response = kinesis_client.get_records(ShardIterator=record_response['NextShardIterator'],
                                                 Limit=2)
    if len(record_response['Records']) > 0:
        for item in record_response['Records']:
            print(json.loads(item['Data']))

    # wait for 5 seconds
    time.sleep(5)
