import unittest
import requests
import json
from chalicelib import config


class AppTestCase(unittest.TestCase):
    def setUp(self):
        self.api_url = 'https://4r79gzw12g.execute-api.us-east-2.amazonaws.com/api'
        self.settings = config.get_config()

    def test_echo(self):
        response = requests.get(self.api_url + '/echo').json()
        self.assertEqual(response['name'], 'data-sync-intake')
        self.assertEqual(response['version'], "v{0}".format(self.settings['version']))

    def test_stream(self):
        data = json.dumps({
            "action": "insert",
            "key": {"test3": "val3"},
            "payload": {"test1": "value1"}
        })
        response = requests.post(self.api_url, data=data,
                                 headers={'content-type': 'application/json'}).json()
        self.assertIn('requestId', response)

    def test_stream_error_400(self):
        data = json.dumps({
            "action": "insert",
            "key1": {"test3": "val3"},
            "payload": {"test1": "value1"}
        })
        response = requests.post(self.api_url, data=data,
                                 headers={'content-type': 'application/json'})
        self.assertEqual(response.status_code, 400)
