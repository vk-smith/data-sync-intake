from __future__ import print_function
from chalice import Chalice, Response
import boto3
import json
from datetime import datetime
import hashlib
from chalicelib import config
import logging

app = Chalice(app_name='intake')
settings = config.get_config()
app.log.setLevel(settings['log_level'])


@app.route('/echo')
def index():
    return {
        'name': 'data-sync-intake',
        'version': 'v{}'.format(settings['version']),
        'timestamp': datetime.now().isoformat()
    }


@app.route('/', methods=['POST'])
def write_to_stream():
    # This is the JSON body the user sent in their POST request.
    request_json = app.current_request.json_body
    app.log.debug("Request {0}".format(request_json))
    try:
        kinesis_client = boto3.client('kinesis', region_name=settings['region_name'])
        key = hashlib.md5(str(json.dumps(request_json['key'])).encode()).hexdigest()
        put_response = kinesis_client.put_record(StreamName=settings['stream_name'],
                                                 Data=json.dumps(request_json).encode(),
                                                 PartitionKey=key)
        app.log.debug("Response {0}".format(put_response))

    except KeyError as err:
        app.log.error("Error on Kinesis Put record: {0} is not a key in the dictionary".format(err))
        return Response(body={
            'code': 'KeyError',
            'message': "{0} is not a key in the dictionary".format(err),
            'timestamp': datetime.now().isoformat()
        }, status_code=400)

    except Exception as err:
        app.log.error("Failed writing to Kinesis: {0}".format(err))
        return Response(body={
            'code': 'exception-code',
            'message': "{0}".format(err),
            'timestamp': datetime.now().isoformat()
        }, status_code=500)

    return Response(body={
        'requestId': put_response['SequenceNumber'],
        'timestamp': datetime.now().isoformat()
    }, status_code=201)
