```
pip3.6 install awscli
pip3.6 install -r requirements.txt
```
###Credentials

Before you can deploy an application, be sure you have credentials configured
```
aws configure
```
### Deploying to AWS    
```
chalice deploy
```

Application Settings in chalicelib/config.json :
```json
{
  "aws_access_key_id": "AKIAJMGA6BS5HKH7OFDQ",
  "aws_secret_access_key": "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",
  "region_name": "us-east-2",
  "stream_name": "python-stream",
  "shard_count": 1,
  "version": "1.0.0"
}
```
### Create Kinesis Stream
```bash
python3.6 create-stream.py 10 us-east-2
10 - shard count
us-east-2 - region name
```

### Write message to stream

```POST /api - Write message to Kinesis Stream```

Request Payload:
```json
{
  "action": "insert" | "update" | "delete",
  "key": {
    "fieldA": "valueA",
    "fieldB": "valueB"
  },
  "payload": {
    "test1": "value1"
  }
}
```
Sample Request:
```
curl -H "Content-Type: application/json" -XPOST 'https://4r79gzw12g.execute-api.us-east-2.amazonaws.com/api' -d '{
  "action": "insert",
  "key": {
    "fieldA": "valueA",
    "fieldB": "valueB"
  },
  "payload": {
    "test1": "value1"
  }
}'
```
Response: 
```json
{"requestId": "49579998249320546027703728126722102982467538727857029122", "timestamp": "2017-12-21T05:44:40.541981"}
```

HTTP Response status codes:
```
201 - The request has succeeded.
400 - Invalid Url or Payload incorrect.
500 - Bad Request. If we could not record to Kinesis Stream.
```

### Unittest
```bash
python3.6 -m unittest tests/test_app.py
```
